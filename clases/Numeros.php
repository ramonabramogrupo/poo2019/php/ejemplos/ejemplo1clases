<?php

namespace clases;
class Numeros extends Vector{
    private $media;
    
    public function getMedia() {
        return $this->media;
    }

    private function setMedia() {
        $this->media = array_sum($this->getValores())/count($this->getValores());
    }

    /**
     * forma explicada en clase
     */
    private function setModa1(){
        $salida= array_count_values($this->getValores());
        $repeticionesMaximas=max($salida); 
        $this->moda=[];
        foreach ($salida as $numeros=>$repes) {
            if($repes==$repeticionesMaximas){
                $this->moda[]=$numeros;
            }
        }
    }
    
    /** 
     * otra forma de calcular la moda
     */
    private function setModa(){
        $repeticiones= array_count_values($this->getValores());
        $repeticionesMaximas=max($repeticiones); 
        $this->moda= array_keys(array_filter($repeticiones, function($repe,$numero)use($repeticionesMaximas){
            return ($repe==$repeticionesMaximas);
        },ARRAY_FILTER_USE_BOTH));
    }
    
    
    public function setValores($valores) {
        parent::setValores($valores);
        $this->calculos();
    }
    
    private function calculos(){
        $this->setModa();
        $this->setMedia ();
    }
    
    public function __construct($valores = []) {
        parent::__construct($valores);
        $this->calculos();
    }
        
}
