<?php

// antes de php5
/*function __autoload($nombre_clase) {
    include $nombre_clase . '.php';
}*/

// en php5

spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});


use clases\Vector;
use clases\Numeros;

$a=new Vector([1,1,1,2,2,2,3,3,3,3,3,3]);
$a->imprimirValores();
echo "El valor maximo es : " . $a->getMaximo();
echo "La moda es : " ;
var_dump($a->getModa());


$b=new Numeros([1,1,1,2,2,2,3,3,3,3,3,4,2,2,2,2,2]);
$b->imprimirValores();
echo "La moda es : " ;
var_dump($b->getModa());
var_dump($b);

$b->setValores([10,10,2,2,3]);
$b->imprimirValores();
echo "La moda es : " ;
var_dump($b->getModa());
var_dump($b);
?>

